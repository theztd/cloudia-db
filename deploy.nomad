variable "fqdn" {
  type = string
  default = "db-cloudia.fejk.net"
}

variable "dcs" {
  type = list(string)
  default = ["dc1", "devel"]
}

variable "image" {
  type = string
  default = "cockroachdb/cockroach:latest-v21.2"
}

job "__JOB_NAME__" {
    datacenters = var.dcs

    group "db" {
        count = 1
        
        network {
            mode = "bridge"
            port "console" { to = 8080 }
            port "db" { to = 5432 }
        }
        

        service {
          name = "${JOB}-db"
          port = "db"

          connect {
            sidecar_service {}
          }
        }

        service {
            name = "${JOB}-console"

            tags = [
                "public",
                "traefik.enable=true",
                "traefik.http.routers.${NOMAD_JOB_NAME}-http.rule=Host(`${var.fqdn}`)",
            ]

            port = "console"

         }

        task "postgres" {
          driver = "docker"

          config {
            image = var.image

            ports = [
              "db"
            ]

            labels {
              group = "postgres"
            }
          }

          env {
            POSTGRES_PASSWORD = "CloudiaPAssword123"
            POSTGRES_USER = "cloudia"
            POSTGRES_DB = "cloudia"
          }

          resources {
            cpu = 400
            memory = 64
            memory_max = 256
          }
        
        } # END task postgres

        task "adminer" {
          driver = "docker"

          config {
            image = "adminer:latest"

            ports = [
              "console"
            ]

            labels {
              group = "adminer"
            }
          }

          resources {
            cpu = 200
            memory = 32
            memory_max = 256
          }
        
        }


    } # END group db

}
