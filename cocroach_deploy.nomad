variable "fqdn" {
  type = string
  default = "db-cloudia.fejk.net"
}

variable "dcs" {
  type = list(string)
  default = ["dc1", "devel"]
}

variable "image" {
  type = string
  default = "cockroachdb/cockroach:latest-v21.2"
}

job "__JOB_NAME__" {
    datacenters = var.dcs

    group "db" {
        count = 1
        
        network {
            port "console" { to = 8080 }
            port "db" { to = 26257 }
        }
        

        service {
          name = "${JOB}-db"
          port = "db"
        }

        service {
            name = "${JOB}-console"

            tags = [
                "public",
                "traefik.enable=true",
                "traefik.http.routers.${NOMAD_JOB_NAME}-http.rule=Host(`${var.fqdn}`)",
            ]

            port = "console"

         }

        task "cocroach" {
          driver = "docker"

          config {
            image = var.image
            args = [
              "start-single-node",
              "--insecure"
            ]

            ports = [
              "db",
              "console"
            ]

            labels {
              group = "cockroach"
            }
          }

          resources {
            cpu = 800
            memory = 128
            memory_max = 256
          }
        
        }

    } # END group db

}
